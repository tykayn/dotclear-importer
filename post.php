<?php

// take post infos


require('logic/config.php');
require('logic/connect.php');



require( 'tpl/head.php' );


// take posts
// check if wordpress posts exist
// if not, create them
// link to wp categories
// link to wp user
// link to wp tags
require( 'logic/find_posts.php' );



// take users
// check if wordpress users exist
// if not, create them
require( 'logic/find_users.php' );
// take categories
// check if wordpress categories exist
// if not, create them
require( 'logic/find_cats.php' );
// take tags
// check if wordpress tags exist
// if not, create them

require( 'logic/find_tags.php' );
echo ' < br />  récupération des tags';

// find posts of the choosen blog_id with custom prefix
$sql = 'SELECT * FROM `dc_meta` WHERE meta_type = "tag" ORDER BY meta_id DESC';
try {
	$result = $dc_bdd->query($sql);
} catch (PDOException $e) {
	echo 'Echec de la connexion : '.$e->getMessage();
	exit;
}

$result->setFetchMode(PDO::FETCH_OBJ);
$tags = [];
$tagsTimes = []; // number of times a tag is found

echo "<br/>résultats des posts:";
while ($donnees = $result->fetch()) {
//	var_dump($donnees);
	if (!isset( $tagsTimes[ $donnees->meta_id ] )) {
		$tagsTimes[ $donnees->meta_id ] = 0;
	}
	if (!isset( $tags[ $donnees->post_id ] )) {
		$tags[ $donnees->post_id ] = [];
	}
	$tags[ $donnees->post_id ][] = $donnees->meta_id;
	$tagsTimes[ $donnees->meta_id ]++;
}
$result->closeCursor();

echo " <div class='well'>
 <h2>".count($tags)." tags</h2>
 les 10 plus fréquents:";
// montrer le nombre de fois qu'un tag est utilisé
arsort($tagsTimes);
foreach (array_slice($tagsTimes, 0, 10) as $tagsTime => $counting) {
	echo "<br> - ".$tagsTime.' x '.$counting;
}

// post et tags reliés
//foreach ($tags as $k=>$v) {
//	echo "<br>- ".$k.' : ';
//	foreach ($v as $item) {
//		echo '/ '.$item;
//	}
//}
echo " </div>";


// rapport des objets créés
// rapport des objets non créés
//require( 'logic/reports.php' );
?>
<h2>Rapports :</h2>
<?php

//print_r($reports);
echo $GLOBALS['reports'] ; ?>

<?php
require( 'tpl/foot.php' );
?>
